SOAL 1
CREATE DATABASE myshop;

SOAL 2
USE myshop;
CREATE TABLE users(id INTEGER AUTO_INCREAMENT PRIMARY KEY,name VARCHAR(225),email VARCHAR(225),password VARCHAR(225));
CREATE TABLE items(id INTEGER AUTO_INCREAMENT PRIMARY KEY,name VARCHAR(225),description VARCHAR(225),price INTEGER,stock INTEGER,category_id INTEGER);
CREATE TABLE categories(id INTEGER AUTO_INCREAMENT PRIMARY KEY,name VARCHAR(225));

SOAL 3
INSERT INTO users(name, email, password) VALUES("John Doe", "john@doe.com", "john123");
INSERT INTO users(name, email, password) VALUES("Jane Doe", "jane@doe.com", "jenita123");
INSERT INTO categories(name) VALUES("gadget");
INSERT INTO categories(name) VALUES("cloth");
INSERT INTO categories(name) VALUES("men");
INSERT INTO categories(name) VALUES("women");
INSERT INTO categories(name) VALUES("branded");
INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
INSERT INTO items(name, description, price, stock, category_id) VALUES("Uniklooh", "baju keren dari brand terkenal", 500000, 50, 2);
INSERT INTO items(name, description, price, stock, category_id) VALUES("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

SOAL 4
SELECT id, name, email FROM users;

SELECT * FROM items WHERE name LIKE '%uniklo%';
SELECT * FROM items WHERE price > 1000000;

SELECT items.name, description, price, stock, category_id, categories.name FROM items JOIN categories ON items.category_id = categories.id;

SOAL 5
UPDATE items SET price = 2500000 WHERE name = 'Sumsang b50';
